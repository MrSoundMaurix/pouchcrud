var PouchDB = require('pouchdb');
PouchDB.plugin(require('relational-pouch'));
PouchDB.plugin(require('pouchdb-find'));

class BaseR {
    constructor() {
        this.db = new PouchDB('agendaprueba6');

        this.db.setSchema([
            {
                singular: 'author',
                plural: 'authors',
                relations: {
                    'books': { hasMany: 'book' }
                }

            },
            {
                singular: 'book',
                plural: 'books',
                relations: {
                    'author': { belongsTo: 'author' }
                }
            }
        ]);

    }

    async GuardarTarea(objeto) {
        try {
            console.log(`este es el objeto guardado `, objeto);
            let resp = await this.db.rel.save('author', objeto);

            return resp;
        } catch (error) {
            console.error(error);
        }
    }
    async GuardarLink(objeto) {
        try {
            console.log("es el objeto-link",objeto);
            let resp = await this.db.rel.save('book', {
                title:objeto.link_value,
                id: objeto.id_link,
                author: objeto.author
            });
            return resp;
        } catch (error) {
            console.error(error);
        }
    }
    async Encontrar() {
        try {
            let resp = await this.db.rel.find('author');

            return resp;
        } catch (error) {
            console.error(error);
        }
    }
    async Encontrarlinks() {
        try {
            let resp = await this.db.rel.find('book');

            return resp;
        } catch (error) {
            console.error(error);
        }
    }
    async EncontrarTarea() {
        try {
            let resp = await this.db.rel.find('author');
            console.log(`render encontrar`, resp.authors.length);
            return resp.authors.length;
        } catch (error) {
            console.error(error);
        }
    }
    async EncontrarTareaEsp(id) {
        try {
            let resp = await this.db.rel.find('author', id);
            console.log(`encontrando tarea hasmany`, resp);
            return resp;
        } catch (error) {
            console.log(error);
        }

    }
    async Eliminar(id, rev) {
        try {

            let resp = await this.db.rel.del('author', {
                id: id,
                rev: rev
            });
            console.log("find de base r", resp);
            return resp;
        } catch (error) {
            console.error(error);
        }

    }
    async Actualizar(id, rev, objeto) {
        try {
            console.log(`render actuaizar`, objeto.detalle)
            let resp = await this.db.rel.save('author', {
                id: id,
                rev: rev,
                fecha: objeto.fecha,
                detalle: objeto.detalle,
                actividad: objeto.actividad,
                progreso: objeto.progreso
            });
            console.log("find de base r", resp);
            return resp;
        } catch (error) {
            console.error(error);
        }
    }
    async allbdd() {
        try {
            var result = await this.db.allDocs({
                include_docs: true,
                attachments: true
            });
            return result.total_rows;
        } catch (err) {
            console.log(err);
        }
    }

}
exports.BaseR = BaseR;