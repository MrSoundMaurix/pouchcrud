//const bd =require("pouchdb");
const PouchDB = require('PouchDB');

class BaseD {
    constructor() {
        this.base = new PouchDB('agenda2');
    }


    async Insertar(objeto) {
        try {
            let response = await this.base.post(objeto);
            console.log(response);
            return response;
        } catch (err) {
            console.error(err);
        }
    }

    async Obtener() {
        try {
            let result = await this.base.allDocs({
                include_docs: true,
                attachments: true
            });
            return result;
        } catch (err) {
            console.error(err);
        }
    }

    async ObtenerRegistro(id) {
        let doc = await this.base.get(id);
        return doc;

    } catch (err) {
        console.error(err);
    }

    async EliminarD(id) {
        try {
            let doc = await this.base.get(id);
            let response = await this.base.remove(doc);
            return response;
        } catch (err) {
            console.log(err);
        }
    }

    async EditarD(objeto){
        try {
            let result = await this.base.bulkDocs(objeto);
            return result;

          } catch (err) {
            console.error(err);
          }  
    } 

    async EditarSegundo(id){
    this.base.get(id).then(function (doc) {
        // update their age
        doc.age = 4;
        // put them back
        return db.put(doc);
      }).then(function () {
        // fetch mittens again
        return db.get(id);
      }).then(function (doc) {
        console.log(doc);
      });
    }


    async EditarTercero(id,rev,objeto){
        try{
            let doc =await this.base.get(id);
            doc.fecha=objeto.fecha;
            doc.actividad=objeto.actividad;
            doc.progreso=objeto.progreso;
            doc.detalle=objeto.detalle;
            doc._id=objeto._id;
            doc._rev=objeto._rev;
            console.log("---",objeto.progreso);
            let res=await this.base.put({
                _id:id,
                _rev:rev,
                progreso:objeto.progreso,
                actividad:objeto.actividad,
                detalle:objeto.detalle,
                fecha:objeto.fecha
             
            });
            
            return res;
        }catch(err){
            console.error(err);
        }
    }

}

exports.BaseD = BaseD;